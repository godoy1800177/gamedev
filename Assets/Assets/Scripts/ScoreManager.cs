using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance;
    public Text scoreText;
    public Text highscoreText;

    int score = 1;
    int highscore = 0;
    
    public void Awake(){
        instance = this;

    }

    // Start is called before the first frame update
    void Start()
    {
        scoreText.text = score.ToString() + "POINTS";
        highscoreText.text = "HIGHSCORE:" + highscore.ToString();
    }



    public void addPoint(){
        score+=1;
        scoreText.text = score.ToString() + "POINTS";
    }
    // Update is called once per frame
   
}
